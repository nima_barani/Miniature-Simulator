﻿using System.Windows.Forms;

namespace MiniatureSimulator.Models
{
    class Registers
    {
        public static int[] registers = new int[16];
        public static int[] usedRegisters = new int[16];
        public static int readData1, readData2;
        public static int totalSum = 0;

        public static void ReadOperation(int regnum1, int regnum2)
        {
            readData1 = ReadData(regnum1);
            readData2 = ReadData(regnum2);
        }
        public static void WriteBack(int regwritenum, int data, int regwrite)
        {
            if (regwrite == 1)
            {
                WriteData(regwritenum, data);
            }
        }

        public static int ReadData(int regnum)
        {
            usedRegisters[regnum]++;
            totalSum++;
            return registers[regnum];
        }

        public static void WriteData(int regnum, int data)
        {
            usedRegisters[regnum]++;
            totalSum++;
            registers[regnum] = data;
        }
    }
}
