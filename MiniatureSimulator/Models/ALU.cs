﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniatureSimulator
{
    class ALU
    {
        public static int ALUResult;
        public static int zero = 0;

        public static void ALUOperation(int src1, int src2, String ALUOp)
        {
            if (ALUOp == "000")
            {
                ALUResult = Add(src1, src2);
            }
            if (ALUOp == "001")
            {
                ALUResult = Sub(src1, src2);
                SetZero(src1, src2);
            }
            if (ALUOp == "010")
            {
                ALUResult = Slt(src1, src2);
            }
            if (ALUOp == "011")
            {
                ALUResult = Or(src1, src2);
            }
            if (ALUOp == "100")
            {
                ALUResult = Nand(src1, src2);
            }
        }

        static int Add(int src1, int src2)
        {
            return src1 + src2;
        }

        static int Sub(int src1, int src2)
        {
            return src1 - src2;
        }

        static int Or(int src1, int src2)
        {
            return src1 | src2;
        }

        static int Nand(int src1, int src2)
        {
            return ~(src1 & src2);
        }

        static int Slt(int src1, int src2)
        {
            if (src1 < src2)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        static void SetZero(int src1, int src2)
        {
            if (src1 == src2)
            {
                zero = 1;
            }
            else
            {
                zero = 0;
            }
        }
    }
}
