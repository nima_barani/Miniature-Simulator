﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniatureSimulator.Models
{
    class Mux
    {
        public int output;
        public Mux(int input1, int input0, int control)
        {
            if (control == 1)
                this.output = input1;
            else
                this.output = input0;
        }
    }
}
