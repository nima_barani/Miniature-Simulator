﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniatureSimulator.Models
{
    class DataMemory
    {
        public static int[] dataMemory = new int[16384];
        public static int returnedData;
        public static int usedMemoryOffset = 0;

        public static void MemoryOperation(int address, int data, int memread, int memwrite)
        {
            if (memread == 1)
            {
                returnedData = ReadData(address);
            }
            if (memwrite == 1)
            {
                WriteData(address, data);
            }
        }

        public static int ReadData(int address)
        {
            usedMemoryOffset++;
            return dataMemory[address];
        }

        public static void WriteData(int address, int data)
        {
            usedMemoryOffset++;
            dataMemory[address] = data;
        }
    }
}
