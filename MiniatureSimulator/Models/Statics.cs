﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniatureSimulator.Models
{
    public class Statics
    {
        public int totalSum = 0;
            public Dictionary<string, string> registers = new Dictionary<string, string> {
                {"PC = ", PC.pc.ToString() },
                {"R0 = ", Registers.registers[0].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[0] * 100) / Registers.totalSum + " %"},
                {"R1[SP] = ", Registers.registers[1].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[1] * 100) / Registers.totalSum + " %"},
                {"R2 = ", Registers.registers[2].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[2] * 100) / Registers.totalSum + " %"},
                {"R3 = ", Registers.registers[3].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[3] * 100) / Registers.totalSum + " %"},
                {"R4 = ", Registers.registers[4].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[4] * 100) / Registers.totalSum + " %"},
                {"R5 = ", Registers.registers[5].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[5] * 100) / Registers.totalSum + " %"},
                {"R6 = ", Registers.registers[6].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[6] * 100) / Registers.totalSum + " %"},
                {"R7 = ", Registers.registers[7].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[7] * 100) / Registers.totalSum + " %"},
                {"R8 = ", Registers.registers[8].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[8] * 100) / Registers.totalSum + " %"},
                {"R9 = ", Registers.registers[9].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[9] * 100) / Registers.totalSum + " %"},
                {"R10 = ", Registers.registers[10].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[10] * 100) / Registers.totalSum + " %"},
                {"R11 = ", Registers.registers[11].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[11] * 100) / Registers.totalSum + " %"},
                {"R12 = ", Registers.registers[12].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[12] * 100) / Registers.totalSum + " %"},
                {"R13 = ", Registers.registers[13].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[13] * 100) / Registers.totalSum + " %"},
                {"R14 = ", Registers.registers[14].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[14] * 100) / Registers.totalSum + " %"},
                {"R15 = ", Registers.registers[15].ToString("X").PadLeft(8, '0') + " | " + (Registers.usedRegisters[15] * 100) / Registers.totalSum + " %"}
            };
    }
}

