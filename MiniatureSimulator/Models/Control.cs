﻿using System;
using ExtensionMethods;

namespace MiniatureSimulator.Models
{
    class ControlUnit
    {
        public static int RegDst, RegWrite, Jump, Branch, ALUSrc, MemRead, MemToReg, MemWrite, Lui, Jalr;
        //public static String ALUOp = null;

        //public static void SetControls(String opcode)
        //{
        //    RegDst = 0;
        //    RegWrite = 0;
        //    Jump = 0;
        //    Branch = 0;
        //    ALUSrc = 0;
        //    MemRead = 0;
        //    MemToReg = 0;
        //    MemWrite = 0;
        //    Lui = 0;
        //    Jalr = 0;
        //    ALUOp = null;

        //    if (opcode == "0000" || opcode == "0001" || opcode == "0010" || opcode == "0011" || opcode == "0100")
        //    {   // Rtypes
        //        RegDst = 1;
        //        RegWrite = 1;
        //    }
        //    if (opcode == "0101" || opcode == "0110" || opcode == "0111")
        //    {   // addi, slti, ori
        //        RegWrite = 1;
        //        ALUSrc = 1;
        //    }
        //    if (opcode == "1000")
        //    {   //lui
        //        Lui = 1;
        //        RegWrite = 1;
        //    }
        //    if (opcode == "1001")
        //    {   //lw
        //        RegWrite = 1;
        //        MemRead = 1;
        //        MemToReg = 1;
        //        ALUSrc = 1;
        //    }
        //    if (opcode == "1010")
        //    {   //sw
        //        MemWrite = 1;
        //        ALUSrc = 1;
        //    }
        //    if (opcode == "1011")
        //    {
        //        Branch = 1; //beq
        //    }
        //    if (opcode == "1100")
        //    {
        //        Jalr = 1; //jalr
        //        RegWrite = 1;
        //    }
        //    if (opcode == "1101")
        //    {
        //        Jump = 1; // j
        //    }

        //    //adjust ALUOp
        //    if (opcode == "0000" || opcode == "0101" || opcode == "1001" || opcode == "1010")
        //    {
        //        ALUOp = "000"; //addOp
        //    }
        //    if (opcode == "0001" || opcode == "1011")
        //    {
        //        ALUOp = "001"; //subOp
        //    }
        //    if (opcode == "0010" || opcode == "0110")
        //    {
        //        ALUOp = "010"; //sltOp
        //    }
        //    if (opcode == "0011" || opcode == "0111")
        //    {
        //        ALUOp = "011"; //orOp
        //    }
        //    if (opcode == "0100")
        //    {
        //        ALUOp = "100"; //nandOp
        //    }
        //}

        //static int RegDst, RegWrite, Jump, Branch, ALUSrc, MemRead, MemToReg, MemWrite, Lui, Jalr;
        static int ALUOp0, ALUOp1, ALUOp2;
        public static string ALUOp;
        public static string binaryOp;
        public static void SetControls(string opcode)
        {
            binaryOp = opcode;
            RegDst = (not(Opcode(3)) & not(Opcode(2)) & not(Opcode(1)) & not(Opcode(0))
                    | (not(Opcode(3)) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0))
                    | not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & not(Opcode(1)) & not(Opcode(0)));

            RegWrite = (not(Opcode(3)) & not(Opcode(2)) & not(Opcode(1)) & not(Opcode(0)))
                    | (not(Opcode(3)) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0))
                    | (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & not(Opcode(1)) & not(Opcode(0)))
                    | (not(Opcode(3)) & Opcode(2) & not(Opcode(1)) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & Opcode(0))
                    | (Opcode(3) & not(Opcode(2)) & not(Opcode(1)) & not(Opcode(0)))
                    | (Opcode(3) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0))
                    | (Opcode(3) & Opcode(2) & not(Opcode(1)) & not(Opcode(0)));

            ALUSrc = (not(Opcode(3)) & Opcode(2) & not(Opcode(1)) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & Opcode(0))
                    | (Opcode(3) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0))
                    | (Opcode(3) & not(Opcode(2)) & Opcode(1) & not(Opcode(0)));

            MemRead = MemToReg = (Opcode(3) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0));

            MemWrite = (Opcode(3) & not(Opcode(2)) & Opcode(1) & not(Opcode(0)));

            Branch = (Opcode(3) & not(Opcode(2)) & Opcode(1) & Opcode(0));

            Jump = (Opcode(3) & Opcode(2) & not(Opcode(1)) & Opcode(0));

            Jalr = (Opcode(3) & Opcode(2) & not(Opcode(1)) & not(Opcode(0)));

            Lui = (Opcode(3) & not(Opcode(2)) & not(Opcode(1)) & not(Opcode(0)));

            ALUOp2 = (not(Opcode(3)) & Opcode(2) & not(Opcode(1)) & not(Opcode(0)));

            ALUOp1 = (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & not(Opcode(0)))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & Opcode(0));

            ALUOp0 = (not(Opcode(3)) & not(Opcode(2)) & not(Opcode(1)) & Opcode(0))
                    | (not(Opcode(3)) & not(Opcode(2)) & Opcode(1) & Opcode(0))
                    | (not(Opcode(3)) & Opcode(2) & Opcode(1) & Opcode(0))
                    | (Opcode(3) & not(Opcode(2)) & Opcode(1) & Opcode(0));

            ALUOp = ALUOp2.ToString() + ALUOp1.ToString() + ALUOp0.ToString();
        }

        public static int Opcode(int i)
        {
            return binaryOp.Reverse()[i] - 48;
        }

        public static int not(int c)
        {
            if (c == 0)
                return 1;
            else
                return 0;
        }
    }
}
