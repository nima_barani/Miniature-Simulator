﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniatureSimulator.Models
{
    class Adder
    {
        public int output;
        public Adder(int input1, int input2)
        {
            this.output = input1 + input2;
        }
    }
}
