﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExtensionMethods;

namespace MiniatureSimulator.Models
{
    public class InstructionMemory
    {
        public static string[] instructionMemory = new string[16384];
        public static string binaryOp, binaryRs, binaryRt, binaryRd, binaryImm;
        public static int rs, rt, rd, imm;

        public static void InstructionMemoryOperation(int pc)
        {
            binaryOp = getBinaryOp(pc);
            binaryRs = getBinaryRs(pc);
            binaryRt = getBinaryRt(pc);
            binaryRd = getBinaryRd(pc);
            binaryImm = getBinaryImm(pc);

            rs = Convert.ToInt32(binaryRs, 2);
            rt = Convert.ToInt32(binaryRt, 2);
            rd = Convert.ToInt32(binaryRd, 2);
            imm = binaryImm.SignExtention();   // including negative numbers . it works just for immediate
        }

        public static String getBinaryOp(int pc)
        {
            return instructionMemory[pc].Substring(4, 4);
        }

        public static String getBinaryRs(int pc)
        {
            return instructionMemory[pc].Substring(8, 4);
        }

        public static String getBinaryRt(int pc)
        {
            return instructionMemory[pc].Substring(12, 4);
        }

        public static String getBinaryRd(int pc)
        {
            return instructionMemory[pc].Substring(16, 4);
        }

        public static String getBinaryImm(int pc)
        {
            return instructionMemory[pc].Substring(16, 16);
        }

    }
}
