﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniatureSimulator.Models;
using ExtensionMethods;

namespace MiniatureSimulator
{
    public partial class MainView : Form
    {
        public static MainView form = new MainView();
        public MainView()
        {
            InitializeComponent();

        }

        private void Open_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string directoryPath = Path.GetFullPath(openFileDialog1.FileName);
                Assembler.Nima.readFile(directoryPath);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            string halt = "00001110000000000000000000000000";
            if (InstructionMemory.instructionMemory[PC.pc] != halt)
            {
                Program.form.richTextBox2.HighlightLine(PC.pc, Color.Yellow);
                Program.runOneCycle();
            }
            else
                MessageBox.Show("There are no more instructions to run", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
            label1.Text = "Executed Instructions: " + Program.executionTime;
            label2.Text = "Memory Usage: " + (Convert.ToDouble(DataMemory.usedMemoryOffset * 100)) / Convert.ToDouble(16384) + "%";
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            string halt = "00001110000000000000000000000000";
            while (InstructionMemory.instructionMemory[PC.pc] != halt)
            {
                Program.runOneCycle();
            }
            label1.Text = "Executed Instructions: " + Program.executionTime ;
            label2.Text = "Memory Usage: " + (Convert.ToDouble(DataMemory.usedMemoryOffset * 100)) / Convert.ToDouble(16384) + "%";
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            Assembler.Nima.SaveFile();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Assembler.Nima.stopExecute();
            label1.Text = "Executed Instructions: 0";
        }
    }
}
