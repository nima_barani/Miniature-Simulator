﻿using MiniatureSimulator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtensionMethods;
using System.Data;
using System.Linq.Expressions;

namespace MiniatureSimulator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static MainView form = new MainView();

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(form);
        }
        public static int executionTime = 0;
        public static void runOneCycle()
        {          
            if (InstructionMemory.instructionMemory[PC.pc].isInstruction())
            {
                executionTime++;
                InstructionMemory.InstructionMemoryOperation(PC.pc);
                ControlUnit.SetControls(InstructionMemory.binaryOp);
                Mux RegDstMux = new Mux(InstructionMemory.rd, InstructionMemory.rt, ControlUnit.RegDst);
                Registers.ReadOperation(InstructionMemory.rs, InstructionMemory.rt);
                Mux AluSrcMux = new Mux(InstructionMemory.imm, Registers.readData2, ControlUnit.ALUSrc);
                ALU.ALUOperation(Registers.readData1, AluSrcMux.output, ControlUnit.ALUOp);
                DataMemory.MemoryOperation(ALU.ALUResult, Registers.readData2, ControlUnit.MemRead, ControlUnit.MemWrite);
                Mux MemToRegMux = new Mux(DataMemory.returnedData, ALU.ALUResult, ControlUnit.MemToReg);
                Adder pcAdder = new Adder(PC.pc, 1);
                int pcAdderoutput = pcAdder.output;
                Adder branchAdder = new Adder(pcAdderoutput, InstructionMemory.imm);
                Mux BranchMux = new Mux(branchAdder.output, pcAdderoutput, ControlUnit.Branch & ALU.zero);
                Mux jumpMux = new Mux(InstructionMemory.imm, BranchMux.output, ControlUnit.Jump);
                Mux JalrMux = new Mux(pcAdderoutput, MemToRegMux.output, ControlUnit.Jalr);
                Mux LuiMux = new Mux(Convert.ToInt32(InstructionMemory.binaryImm.ShiftLeft16(), 2), JalrMux.output, ControlUnit.Lui); //here
                Registers.WriteBack(RegDstMux.output, LuiMux.output, ControlUnit.RegWrite);
                Mux PcInput = new Mux(Registers.readData1, jumpMux.output, ControlUnit.Jalr);
                PC.pc = PcInput.output;
                Statics obj = new Statics();
                form.richTextBox1.Clear();
                foreach (KeyValuePair<string, string> register in obj.registers)
                {
                    form.richTextBox1.AppendText(register.Key + register.Value + Environment.NewLine);
                }
            }
            else
            {
                Adder pcAdder = new Adder(PC.pc, 1);
                PC.pc = pcAdder.output;
            }
        }
    }
}
