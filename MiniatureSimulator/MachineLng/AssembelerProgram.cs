﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AssembelerExtensionMethods;
using MiniatureSimulator.Models;

namespace Assembler
{
    static class Labels
    {
        public static Dictionary<string, int> labels = new Dictionary<string, int>();
    }

    public static class Nima
    {
        static List<string> instructionMemory = new List<string>();
        static List<string> memory = new List<string>();
        static string[] commands;
        static List<string[]> parseCommands = new List<string[]>();
        static Dictionary<int, int> spaces = new Dictionary<int, int>();

        internal static void readFile(string directoryPath)
        {
            try
            {
                using (StreamReader sr = new StreamReader(directoryPath))
                {
                    List<string> _commands = new List<string>();
                    string command;
                    while ((command = sr.ReadLine()) != null)
                    {
                        _commands.Add(command);
                        MiniatureSimulator.Program.form.richTextBox2.AppendText(command + Environment.NewLine);
                        //MessageBox.Show(command);
                    }
                    commands = _commands.ToArray();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("The file could not be read:", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            parse();
        }

        static public void parse()
        {
            char[] separatingChars = { ' ', ',', '\t'};
            int i = 0; //line number or instruction number
            foreach (string command in commands)
            {
                try
                {

                    string[] words = command.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);
                    if (words.Length > 5)
                    {
                        //throw an error
                    }
                    if (words[0].isInstruction())
                    {
                        parseCommands.Add(words);
                    }
                    else if (words[1].isInstruction())
                    {
                        if (words[0].Length > 6)
                        {
                            MessageBox.Show("Label is more than 6 characters.", "Wrong Label Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Environment.Exit(0);
                        }
                        if (words[0][0].isNumeric()) {
                            MessageBox.Show("Label starts with number.", "Wrong Label Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Environment.Exit(0);
                        }
                        if (!Labels.labels.ContainsKey(words[0]))
                        {
                            Labels.labels.Add(words[0], i);
                            words = words.Skip(1).ToArray();
                            parseCommands.Add(words);
                        }
                        else
                            MessageBox.Show("Duplicated labels have been detected", "Duplicated Labels", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(words[0] + " is not an opcode!", "Unrecognized Opcode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }
                    i++;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
            }
            builderLoop();
        }

        public static void builderLoop()
        {
            int index = 0;
            foreach (string[] words in parseCommands)
            {
                buildInstructions(words, index);
                index++;
            }
            assemble();
        }

        static void buildInstructions(string[] words, int index)
        {
            string type = words[0].type();
            try
            {
                switch (type)
                {
                    case "R":
                        RFormat r = new RFormat();
                        r.setOpcode(words[0]);
                        r.setRs(words[2]);
                        r.setRt(words[3]);
                        r.setRd(words[1]);
                        memory.Add(r.getMachineCode());
                        break;

                    case "I":
                        IFormat i = new IFormat(words[0]);
                        i.index = index;
                        i.setOpcode(words[0]);
                        if (words[0] == "lui")
                        {
                            i.setRs(null);
                        }
                        else
                        {
                            i.setRs(words[2]);
                        }
                        i.setRt(words[1]); //i'm here Nima
                        if (words[0] == "jalr")
                        {
                            i.setImm(null);
                        }
                        else if (words[0] == "lui")
                        {
                            i.setImm(words[2]);
                        }
                        else
                        {
                            i.setImm(words[3]);
                        }
                        memory.Add(i.getMachineCode());
                        break;

                    case "J":
                        JFormat j = new JFormat(words[0]);
                        j.setOpcode(words[0]);
                        if (words[0] == "j") j.setImm(words[1]);
                        else j.setImm(null);
                        memory.Add(j.getMachineCode());
                        break;

                    case "D":
                        Directive d = new Directive(words[1]);
                        if (words[0] == ".space")
                        {
                            memory.Add("0");
                            spaces.Add(index, d.getD());
                            break;
                        }
                        memory.Add(d.getMachineCode());
                        break;
                }
            }
            catch (IndexOutOfRangeException e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static List<string> _memory = memory;

        static void assemble()
        {
            for (int index = 0; index < memory.Capacity; index++)
            {

                if (spaces.Count > 0)
                {
                    if (index == spaces.ElementAt(0).Key)
                    {
                        for (int k = 0, j = index; k < spaces.ElementAt(0).Value; k++, j++)
                            _memory[j] = "0";
                        spaces.Remove(index);
                    }
                }
                else break;
            }

            int i = 0;
            foreach (string str in _memory)
            {
                string _str = Convert.ToInt32(str, 2).ToString();
                MiniatureSimulator.Program.form.richTextBox3.AppendText(_str + Environment.NewLine);
                InstructionMemory.instructionMemory[i] = str.PadLeft(32, '0');
                DataMemory.dataMemory[i++] = Convert.ToInt32(str, 2);
            }
        }

        static public void SaveFile()
        {
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\program.mc"))
            {
                int i = 0;
                foreach (string str in _memory)
                {
                    //MessageBox.Show(Convert.ToInt32(str, 2).ToString());
                    string _str = Convert.ToInt32(str, 2).ToString();
                    outputFile.WriteLine(_str);
                }
                MessageBox.Show("Code save succesfully", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
        
        static public void stopExecute()
        {
            memory.Clear();
            Array.Clear(commands, 0, commands.Length);
            parseCommands.Clear();
            spaces.Clear();
            Labels.labels.Clear();
            Registers.registers = new int[16];
            InstructionMemory.instructionMemory = new string[16384];
            DataMemory.dataMemory = new int[16384];
            Registers.usedRegisters = new int[16];
            PC.pc = 0;
            MiniatureSimulator.Program.executionTime = 0;
            Statics obj = new Statics();
            DataMemory.usedMemoryOffset = 0;
            MiniatureSimulator.Program.form.richTextBox1.Clear();
            foreach (KeyValuePair<string, string> register in obj.registers)
            {
                MiniatureSimulator.Program.form.richTextBox1.AppendText(register.Key + register.Value + Environment.NewLine);
            }
            MiniatureSimulator.Program.form.richTextBox2.Clear();
            MiniatureSimulator.Program.form.richTextBox3.Clear();
        }


    }

}