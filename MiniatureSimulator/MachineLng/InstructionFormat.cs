﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AssembelerExtensionMethods;

namespace Assembler
{
    class InstructionFormat
    {
        protected bool canUseLabel = false;
        protected byte[] instructions = new byte[32];
        public string machineCode;

        Dictionary<string, string> instructionsOpcode = new Dictionary<string,string>
        {
            { "add", "0000" }, { "sub", "0001" }, {"slt", "0010"}, {"or", "0011"}, {"nand", "0100"}, {"addi", "0101"}, { "slti", "0110" }, { "ori", "0111" }, {"lui", "1000"}, {"lw", "1001"}, {"sw", "1010"}, {"beq", "1011"}, {"jalr", "1100"}, {"j", "1101"}, {"halt", "1110"}
        };

        virtual public void setOpcode(string instruction)
        {
            foreach (KeyValuePair<string, string> instructionOpcode in instructionsOpcode)
            {
                if (instruction == instructionOpcode.Key)
                {
                    machineCode += instructionOpcode.Value;
                    break;
                }
            }
        }
        virtual public void setRs(string rs)
        {
            try
            {
                Int16 _rs = Convert.ToInt16(rs);
                if (_rs > 16 || _rs < 0)
                {
                    MessageBox.Show(rs + " register doesn't exist.", "Wrong Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
                machineCode += _rs.toBinaryString(4);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Input string is not a sequence of digits.");
            }
            catch (OverflowException e)
            {
                MessageBox.Show(rs + "doesn't fit in 16 bits", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        virtual public void setRt(string rt)
        {
            try
            {
                Int16 _rt = Convert.ToInt16(rt);
                if (_rt > 15 || _rt < 0)
                {
                    MessageBox.Show(rt + " register doesn't exist.", "Wrong Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
                machineCode += _rt.toBinaryString(4);
            }
            catch (FormatException)
            {
                Console.WriteLine("Input string is not a sequence of digits.");
                Environment.Exit(0);
            }
            catch (OverflowException)
            {
                MessageBox.Show(rt + "doesn't fit in 16 bits", "Wrong Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
        }
        virtual public void setImm(string imm)
        {
            try
            {
                if (imm.isNumeric())
                {
                    try
                    {
                        Int16 _imm = Convert.ToInt16(imm);
                        if (_imm >= Int16.MinValue && _imm <= Int16.MaxValue)
                        {
                            _imm = Convert.ToInt16(_imm);
                        }
                        machineCode += _imm.toBinaryString(16);
                    }
                    catch (OverflowException)
                    {
                        MessageBox.Show(imm + " doesn't fit in 16 bits.", "Offset Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }
                }
                else if (this.canUseLabel)
                {
                    foreach (KeyValuePair<string, int> label in Labels.labels)
                    {

                        if (imm == label.Key)
                        {
                            machineCode += label.Value.toBinaryString(16);
                            break;
                        }
                    }
                    //throw error "wrong label"
                }
                else
                {
                    MessageBox.Show(imm + "doesn't fit in 16 bits.", "Offset Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
        }
        public string getMachineCode()
        {
            return this.machineCode;
        }
    }


    class RFormat : InstructionFormat
    {
        public RFormat()
        {
            this.machineCode = "0000";
        }
        public override void setOpcode(string instruction)
        {
            base.setOpcode(instruction);
        }
        public void setRd(string rd)
        {
            try
            {
                Int16 _rd = Convert.ToInt16(rd);
                if (_rd > 15 || _rd < 0)
                {
                    MessageBox.Show(rd + " register doesn't exist.", "Wrong Register", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
                machineCode += _rd.toBinaryString(4);
                machineCode += Convert.ToString(0, 2).PadLeft(12, '0');
            }
            catch (FormatException)
            {
                Console.WriteLine("Input string is not a sequence of digits.");
                Environment.Exit(0);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public override void setRs(string rs)
        {
            base.setRs(rs);
        }
        public override void setRt(string rt)
        {
            base.setRt(rt);
        }
    }

    class IFormat : InstructionFormat
    {
        public string instruction;
        public int index;
        public IFormat(string instruction)
        {
            this.machineCode = "0000";
            if (instruction == "lw" || instruction == "sw" || instruction == "beq")
                this.canUseLabel = true;
            this.instruction = instruction;
        }
        override public void setOpcode(string instruction)
        {
            base.setOpcode(instruction);
        }
        public override void setRt(string rt)
        {
            base.setRt(rt);
        }
        public override void setRs(string rs)
        {
            if (rs == null)
            {
                machineCode += "0000";
                return;
            }
            else base.setRs(rs);
        }
        override public void setImm(string imm)
        {
            if (imm == null)
            {
                machineCode += Convert.ToString(0, 2).PadLeft(16, '0');
                return;
            }
            try
            {
                if (imm.isNumeric())
                {
                    try
                    {
                        Int16 _imm = Convert.ToInt16(imm);
                        if (_imm >= Int16.MinValue && _imm <= Int16.MaxValue)
                        {
                            _imm = Convert.ToInt16(_imm);
                        }
                        machineCode += _imm.toBinaryString(16);
                    }
                    catch (OverflowException)
                    {
                        MessageBox.Show(imm + " doesn't fit in 16 bits.", "Offset Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }
                }
                else if (this.canUseLabel)
                {
                    foreach (KeyValuePair<string, int> label in Labels.labels)
                    {
                        
                        if (imm == label.Key)
                        {
                            if (instruction == "beq")
                            {                            
                                machineCode += (label.Value - (index + 1)).toBinaryString(16);
                            } else
                                machineCode += label.Value.toBinaryString(16);
                            break;
                        }
                    }
                    //throw error "wrong label"
                }
                else
                {
                    MessageBox.Show(imm + "doesn't fit in 16 bits.", "Offset Overflow", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(0);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
        }
    }

    class JFormat : InstructionFormat
    {
        public JFormat(string instruction)
        {
            this.machineCode = "0000";
            if (instruction == "j")
                this.canUseLabel = true;
        }
        override public void setOpcode(string instruction)
        {
            base.setOpcode(instruction);
        }
        override public void setImm(string imm) 
        {
            this.machineCode += "00000000";
            if (imm == null) //check if it is 'halt' instruction
            {
                machineCode += Convert.ToString(0, 2).PadLeft(16, '0');
                return;
            }
            base.setImm(imm); //true because it would be 'j' instruction
        }
    }

    class Directive : InstructionFormat
    {
        private int d;
        public Directive(string d)
        {
            if (d.isNumeric())
            {
                this.d = Convert.ToInt32(d);
                machineCode = d.toBinaryString(1);
            }
            else
            {
                foreach (KeyValuePair<string, int> label in Labels.labels)
                {
                    if (d == label.Key)
                    {
                        this.d = label.Value;
                        if (d != ".space")
                        {
                            machineCode += Convert.ToString(label.Value, 2);
                        }
                        else
                        {
                            machineCode = "0";
                        }
                        break;
                    }
                }
                //throw error "wrong label"
            }
        }
        public int getD()
        {
            return this.d;
        }
    }
}
