﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssembelerExtensionMethods
{
    public static class AssembelerExtensionMethods
    {
        static string[] instructions = { "add", "sub", "slt", "or", "nand", "addi", "slti", "ori", "lui", "lw", "sw", "beq", "jalr", "j", "halt", ".fill", ".space" };
        static string[] RInstructions = { "add", "sub", "slt", "or", "nand"};
        static string[] IInstructions = {"addi", "slti", "ori", "lui", "lw", "sw", "beq", "jalr"};
        static string[] JInstructions = {"j", "halt" };
        static string[] DInstruction = {".fill", ".space"};

        public static string type(this string word)
        {
            if (Array.Exists(RInstructions, element => element == word))
                return "R";
            if (Array.Exists(IInstructions, element => element == word))
                return "I";
            if (Array.Exists(JInstructions, element => element == word))
                return "J";
            if (Array.Exists(DInstruction, element => element == word))
                return "D";
            return null;
        }
        public static bool isInstruction(this String word)
        {
            if (Array.Exists(instructions, element => element == word))
                return true;
            return false; 
        }

        public static bool isNumeric(this string str)
        {
            int n;
            return int.TryParse(str, out n);
        }
        public static bool isNumeric(this char str)
        {
            int n;
            return int.TryParse(str.ToString(), out n);
        }

        public static string toBinaryString(this Int16 value, int width)
        {
            return Convert.ToString(value, 2).PadLeft(width, '0');
        }

        public static string toBinaryString(this int value, int width)
        {
            return Convert.ToString(value, 2).PadLeft(width, '0');
        }

        public static string toBinaryString(this string value, int width)
        {
            int n = Convert.ToInt32(value);
            return Convert.ToString(n, 2).PadLeft(width, '0');
        }

        public static string stringBuilder(this string str, int index, string subString)
        {
            var aStringBuilder = new StringBuilder(str);
            aStringBuilder.Remove(index, subString.Length);
            aStringBuilder.Insert(index, subString);
            return aStringBuilder.ToString();
        }
    }
}
