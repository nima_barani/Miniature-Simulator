﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtensionMethods
{
    public static class ExtensionMethods
    {
        public static bool isNumeric(this string str)
        {
            int n;
            return int.TryParse(str, out n);
        }

        public static string toBinaryString(this Int16 value, int width)
        {
            return Convert.ToString(value, 2).PadLeft(width, '0');
        }

        public static string toBinaryString(this int value, int width)
        {
            return Convert.ToString(value, 2).PadLeft(width, '0');
        }

        public static string toBinaryString(this string value, int width)
        {
            int n = Convert.ToInt32(value);
            return Convert.ToString(n, 2).PadLeft(width, '0');
        }

        public static string stringBuilder(this string str, int index, string subString)
        {
            var aStringBuilder = new StringBuilder(str);
            aStringBuilder.Remove(index, subString.Length);
            aStringBuilder.Insert(index, subString);
            return aStringBuilder.ToString();
        }

        public static string ShiftLeft16(this string str)
        {
            return str.PadRight(32, '0');
        }

        public static int SignExtention (this string str)
        {
            if (str[0] == '0')
                return Convert.ToInt32(str.PadLeft(32, '0'), 2);
            else
                return Convert.ToInt32(str.PadLeft(32, '1'), 2);
        }

        public static bool isInstruction(this string instruction)
        {
            if (Convert.ToInt32(instruction.Remove(19, 13), 2) == 0)
            {
                return false;
            }
            return true;
        }

        public static string Reverse(this string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static void HighlightLine(this RichTextBox richTextBox, int index, Color color)
        {
            richTextBox.SelectAll();
            richTextBox.SelectionBackColor = richTextBox.BackColor;
            var lines = richTextBox.Lines;
            if (index < 0 || index >= lines.Length)
                return;
            var start = richTextBox.GetFirstCharIndexFromLine(index);  // Get the 1st char index of the appended text
            var length = lines[index].Length;
            richTextBox.Select(start, length);                 // Select from there to the end
            richTextBox.SelectionBackColor = color;
        }
    }
}

